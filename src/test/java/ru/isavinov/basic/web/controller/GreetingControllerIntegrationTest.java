package ru.isavinov.basic.web.controller;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class GreetingControllerIntegrationTest {


    @Test
    void greeting(@Autowired TestRestTemplate restTemplate) {

        // Arrange
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cookie", cookie(restTemplate));

        // Act
        ResponseEntity<String> response = restTemplate.exchange("/greeting", HttpMethod.GET, new HttpEntity<>(headers), String.class);

        // Assert
        Assertions.assertThat(response.getBody()).isEqualTo("Hi there");

    }


    public String cookie(TestRestTemplate restTemplate){
        String username = "user";
        String password = "12345";


        MultiValueMap<String, String> form = new LinkedMultiValueMap<>();
        form.set("username", username);
        form.set("password", password);


        ResponseEntity<String> loginResponse = restTemplate.postForEntity(
                "/login",
                new HttpEntity<>(form, new HttpHeaders()),
                String.class);
        return loginResponse.getHeaders().get("Set-Cookie").get(0);
    }


}