package ru.isavinov.basic.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.isavinov.basic.web.model.ToDoItem;
import ru.isavinov.basic.web.service.ToDoItemService;

import java.util.Collection;

@RestController
@RequestMapping("/todo")
public class ToDoItemsController {

    private final ToDoItemService toDoItemService;


    @Autowired
    public ToDoItemsController(ToDoItemService toDoItemService) {
        this.toDoItemService = toDoItemService;
    }

    @GetMapping("/items")
    public Collection<ToDoItem> getToDoItems(){
        return toDoItemService.getToDoItems();
    }

    @GetMapping("/items/{id}")
    public ToDoItem getToDoItem(@PathVariable("id") Long id){
        return toDoItemService.getToDoItem(id);
    }

    @PostMapping("/items")
    public ToDoItem createToDoItem(@RequestBody ToDoItem toDoItem){
        return toDoItemService.saveToDoItem(toDoItem);
    }

    @PutMapping("/items/{id}")
    public void updateToDoItem(@PathVariable("id") Long id, @RequestBody ToDoItem toDoItem){
        toDoItemService.updateToDoItem(id, toDoItem);
    }

    @DeleteMapping("/items/{id}")
    public void deleteToDoItem(@PathVariable("id") Long id){
        toDoItemService.removeToDoItem(id);
    }

}
