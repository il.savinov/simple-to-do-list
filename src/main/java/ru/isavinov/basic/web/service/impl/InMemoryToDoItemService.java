package ru.isavinov.basic.web.service.impl;

import lombok.NonNull;
import org.springframework.stereotype.Service;
import ru.isavinov.basic.web.exceptions.ToDoItemNotFoundException;
import ru.isavinov.basic.web.model.ToDoItem;
import ru.isavinov.basic.web.service.ToDoItemService;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class InMemoryToDoItemService implements ToDoItemService {

    private final Map<Long, ToDoItem> storage;

    private final AtomicLong keyGenerator = new AtomicLong();

    public InMemoryToDoItemService(){
        this.storage=new HashMap<>();
    }

    public InMemoryToDoItemService(Map<Long,ToDoItem> storage){
        this.storage=storage;
    }

    @Override
    public List<ToDoItem> getToDoItems() {
        return storage.values().stream().sorted(Comparator.comparingLong(ToDoItem::getId)).toList();
    }

    @Override
    public ToDoItem getToDoItem(@NonNull Long id) {
        if (!storage.containsKey(id)) {
            throw new ToDoItemNotFoundException();
        }
        return storage.get(id);
    }

    @Override
    public ToDoItem saveToDoItem(@NonNull ToDoItem toDoItem) {
        Long key = keyGenerator.incrementAndGet();
        toDoItem.setId(key);
        storage.put(key, toDoItem);
        return toDoItem;

    }

    @Override
    public void updateToDoItem(@NonNull Long id, @NonNull ToDoItem toDoItem) {
        if (!storage.containsKey(id)) {
            throw new ToDoItemNotFoundException();
        }
        toDoItem.setId(id);
        storage.put(id, toDoItem);
    }

    @Override
    public void removeToDoItem(@NonNull Long id) {
        if (!storage.containsKey(id)) {
            throw new ToDoItemNotFoundException();
        }
        storage.remove(id);
    }
}
