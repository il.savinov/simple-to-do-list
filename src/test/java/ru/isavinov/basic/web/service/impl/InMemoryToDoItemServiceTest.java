package ru.isavinov.basic.web.service.impl;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.isavinov.basic.web.model.ToDoItem;
import ru.isavinov.basic.web.service.ToDoItemService;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

class InMemoryToDoItemServiceTest {

    @Test
    void testSaveToDoItem() {
        ToDoItemService itemService = new InMemoryToDoItemService();

        //Arrange
        ToDoItem item = new ToDoItem();
        item.setTopic("Topic");
        item.setContent("Content");
        item.setIsDone(false);

        //Act
        ToDoItem savedItem = itemService.saveToDoItem(item);

        //Assert
        assertThat(savedItem.getTopic()).isEqualTo(item.getTopic());
        assertThat(savedItem.getContent()).isEqualTo(item.getContent());
        assertThat(savedItem.getIsDone()).isEqualTo(item.getIsDone());

    }

    @Test
    void testGetToDoItem() {
        Map storage = Mockito.mock(Map.class);
        ToDoItemService itemService = new InMemoryToDoItemService(storage);

        //Arrange
        ToDoItem item = new ToDoItem();
        item.setId(1L);
        item.setTopic("Topic");
        item.setContent("Content");
        item.setIsDone(false);

        Mockito.when(storage.get(1L)).thenReturn(item);
        Mockito.when(storage.containsKey(1L)).thenReturn(true);

        //Act
        ToDoItem returnedItem = itemService.getToDoItem(1L);

        //Assert
        assertThat(returnedItem.getTopic()).isEqualTo(item.getTopic());
        assertThat(returnedItem.getContent()).isEqualTo(item.getContent());
        assertThat(returnedItem.getIsDone()).isEqualTo(item.getIsDone());

    }
}