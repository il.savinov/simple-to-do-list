package ru.isavinov.basic.web.service;

import ru.isavinov.basic.web.model.ToDoItem;

import java.util.List;

public interface ToDoItemService {

    /**
     *
     * @return List of existing to-do items
     */
    List<ToDoItem> getToDoItems();

    /**
     *
     * @param id - Unique identificator of the to-do item
     * @return To-Do item with the given identificator
     */
    ToDoItem getToDoItem(Long id);

    /**
     * Create the new to-do item
     * @param toDoItem To-Do item to be saved
     */
    ToDoItem saveToDoItem(ToDoItem toDoItem);

    /**
     * Update the exisitng to-do item
     * @param id Unique identificator of the to-do item
     * @param toDoItem To-Do item to be saved
     */
    void updateToDoItem(Long id, ToDoItem toDoItem);

    /**
     * Remove ToDoItem by the given id
     * @param id Unique identificator of the to-do item
     */
    void removeToDoItem(Long id);

}
