package ru.isavinov.basic.web.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class ToDoItem {

    private Long id;

    @NotBlank
    private String topic;

    @NotBlank
    private String content;

    @NotNull
    private Boolean isDone;

}
