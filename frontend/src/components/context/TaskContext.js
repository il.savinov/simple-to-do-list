import {createContext, useEffect, useState} from "react";

const TaskContext = createContext();
export const TaskProvider = ({ children }) => {



  const [tasks, setTasks] = useState([]
  );

  const [doneTaskList, setDoneTask] = useState([]);
  const [PopUp, setPopUp] = useState({ in: false, item: null });


  const addTasks = (topic, content) => {

    const newTask = {
      isDone: false,
      topic,
      content
    };

    fetch("/todo/items", {
      method: "POST",
      body: JSON.stringify(newTask),
      headers: {
        'Content-Type': 'application/json'
      },
    }).then(response => {
        if (response.ok){
              fetch("/todo/items")
              .then(response => response.json())
              .then(result => setTasks(result))}
        });

  };

  useEffect(() => {
    fetch("/todo/items")
        .then(response => response.json())
        .then(result => setTasks(result));
  }, []);

  useEffect(() => {
    const alldoneTask = tasks.filter((task) => task.isDone === true);
    setDoneTask(alldoneTask);
  }, [tasks]);


  const deleteTask = (id) => {

    fetch(`/todo/items/${id}`, {
      method: "DELETE",
    }).then(response => {
      if (response.ok){
        fetch("/todo/items")
            .then(response => response.json())
            .then(result => setTasks(result))}
    });


  };


  const deleteAll = () => {
    setTasks([]);
    localStorage.setItem("tasks", JSON.stringify([]));
  };


  const doneTask = (id) => {
    const newTask = [...tasks];
    const index = newTask.findIndex((task) => task.id === id);

    fetch(`/todo/items/${id}`, {
      method: "PUT",
      body: JSON.stringify({
        topic: newTask[index].topic,
        content: newTask[index].content,
        isDone: !newTask[index].isDone
      }),
      headers: {
        'Content-Type': 'application/json'
      },
    }).then(response => {
      if (response.ok){
        fetch("/todo/items")
            .then(response => response.json())
            .then(result => setTasks(result))}
    });

  };


  const setId = (id) => {
    const index = tasks.findIndex((task) => task.id === id);
    setPopUp({ in: !PopUp.in, item: tasks[index] });
  };


  const editTask = (text) => {
    const newTask = [...tasks];
    const index = newTask.findIndex((task) => task.id === PopUp.item.id);

    fetch(`/todo/items/${PopUp.item.id}`, {
      method: "PUT",
      body: JSON.stringify({
        topic: text.topic,
        content: text.content,
        isDone: newTask[index].isDone
      }),
      headers: {
        'Content-Type': 'application/json'
      },
    }).then(response => {
      if (response.ok){
        fetch("/todo/items")
            .then(response => response.json())
            .then(result => setTasks(result))}
    });

  };
  return (
    <TaskContext.Provider
      value={{
        tasks,
        addTasks,
        deleteTask,
        doneTask,
        PopUp,
        setPopUp,
        setId,
        deleteAll,
        editTask,
        doneTaskList
      }}
    >
      {children}
    </TaskContext.Provider>
  );
};
export default TaskContext;
